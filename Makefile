# Set container runtime (docker or podman)
CONTAINER_RUNTIME ?= docker

# Set APP_NAME for docker image name
APP_NAME=$(shell docker run --rm -v ./:/app -w /app node:lts-alpine node -e "console.log(require('./package.json').name)")

# Docker build run setup for dev
build:
	$(CONTAINER_RUNTIME) build -t $(APP_NAME)-dev .
run:
	$(CONTAINER_RUNTIME) run -v ./:/app --entrypoint /bin/bash -it $(APP_NAME)-dev
build-run: build run

# Docker build run setup for prod
build-prod:
	$(CONTAINER_RUNTIME) build --target production -t $(APP_NAME) .
run-prod:
	$(CONTAINER_RUNTIME) run $(APP_NAME)
build-run-prod: build-prod run-prod
