# Jukebot

## Description
Discord bot for audio diffusion from youtube and spotify.

## youtube-dl

This bot uses [youtube-dl](https://github.com/ytdl-org/youtube-dl) to download audio.

If you run the bot on a VPS provider, it might require you to authenticate (ex: with youtube, you get error "Sign in to confirm you’re not a bot.").

To do so, please provide a .netrc file, as described [here](https://github.com/ytdl-org/youtube-dl?tab=readme-ov-file#authentication-with-netrc-file).

## TODO List
- [X] Handle music from youtube url
- [ ] Handle music from spotify url
- [X] Basic queue commands (/stop, /resume, /clear, /skip)
- [ ] Server playlists
- [ ] Concurrency between multiple servers
- Extras :
  - [ ] Sounds deformation
  - [ ] Sound deformation saved presets (by server)