import { REST } from '@discordjs/rest';
import { AudioPlayer, AudioPlayerStatus, createAudioPlayer, createAudioResource, NoSubscriberBehavior } from '@discordjs/voice';
import { Routes } from 'discord-api-types/v10';
import { Client, Collection, Events, GatewayIntentBits } from 'discord.js';
import { ClearCommand } from '../commands/clear';
import { NextCommand } from '../commands/next';
import { PauseCommand } from '../commands/pause';
import { PlayCommand } from '../commands/play';
import { PreviousCommand } from '../commands/previous';
import { QueueCommand } from '../commands/queue';
import { ResumeCommand } from '../commands/resume';
import { StopCommand } from '../commands/stop';
import { Config } from '../config/config.type';
import { CommandClient } from '../types/command-client.type';
import { Errors } from '../types/error.enum';
import { QueueItem } from '../types/queue-item.type';
import { YoutubeClient } from './youtube';
import { LoopCommand } from '../commands/loop';
import { Readable } from 'stream';
import { ShuffleCommand } from '../commands/shuffle';

export class Discord {
    client: CommandClient;
    player: AudioPlayer;
    youtubeApi: YoutubeClient;
    queue: QueueItem[] = [];
    currentlyPlaying?: QueueItem = undefined;
    previous: QueueItem[] = [];
    loop = false;

    constructor(private config: Config) {
        // Create a new client instance
        this.client = new Client({
            intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildVoiceStates],
        });

        // Build commands
        const commands = [new PlayCommand(this).get(),
            new PauseCommand(this).get(),
            new ResumeCommand(this).get(),
            new StopCommand(this).get(),
            new NextCommand(this).get(),
            new PreviousCommand(this).get(),
            new ClearCommand(this).get(),
            new QueueCommand(this).get(),
            new LoopCommand(this).get(),
            new ShuffleCommand(this).get(),
        ];

        // Attach them to the client
        this.client.commands = new Collection();
        for (const command of commands) {
            this.client.commands.set(command.data.name, command);
        }

        // Log commands built
        console.log('Commands built:');
        console.log(this.client.commands);

        // Register commands
        const rest = new REST().setToken(config.discord.token);
        try {
            if (!config.discord.testGuildId) {
                this.registerGlobally(rest);
            } else {
                this.registerInTestGuild(rest);
            }
        } catch (error) {
            console.error(error);
        }

        // Init player
        this.player = createAudioPlayer({ debug: true, behaviors: { noSubscriber: NoSubscriberBehavior.Stop } });
        this.player.on('error', (error) => {
            console.error('The player connection encountered an error:' + error.message);
            console.error(error);
        });
        this.player.on('stateChange', (oldState, newState) => {
            if (oldState.status === AudioPlayerStatus.Playing && newState.status === AudioPlayerStatus.Idle) {
                const item = this.queue.shift();
                if (item) {
                    this.playItem(item);
                }
            }
        });

        // Init youtube API
        this.youtubeApi = new YoutubeClient(config);

        this.client.once(Events.ClientReady, () => {
            // Log when the bot is ready
            console.log(`Logged in as ${this.client?.user?.tag}!`);
            console.log(
                `Guilds: ${this.client.guilds.cache
                    .map(g => `${g.name} (${g.id})`)
                    .join(', ')}`,
            );
        });

        // When interaction is a chat input command, get corresponding command and execute it
        this.client.on(Events.InteractionCreate, async (interaction) => {
            if (!interaction.isChatInputCommand()) return;
            const command = (interaction.client as CommandClient).commands?.get(
                interaction.commandName,
            );

            if (!command) {
                console.error(
                    `No command matching ${interaction.commandName} was found.`,
                );
                return;
            }

            try {
                await command.execute(interaction);
            } catch (error) {
                console.error(error);
                if (interaction.replied || interaction.deferred) {
                    await interaction.followUp({
                        content: 'There was an error while executing this command!',
                        ephemeral: true,
                    });
                } else {
                    await interaction.reply({
                        content: 'There was an error while executing this command!',
                        ephemeral: true,
                    });
                }
            }
        });

        // Log on bot error
        this.client.on(Events.Error, (error) => {
            console.error(`The websocket connection encountered an error: ${error.message}`);
        });

        // Login to discord
        this.client.login(this.config.discord.token);
    }

    /**
     * Register commands globally
     */
    private async registerGlobally(rest: REST): Promise<void> {
        if (!this.config.discord) {
            throw new Error('Discord config is missing');
        }
        console.log('Started refreshing application (/) commands.');
        await rest.put(Routes.applicationCommands(this.config.discord.clientId), {
            body: this.client.commands?.map(value => value.data.toJSON()),
        });
        console.log('Successfully reloaded application (/) commands.');
    }

    /**
     * Register commands in test guild for dev testing (instant refresh)
     */
    private async registerInTestGuild(rest: REST): Promise<void> {
        if (!this.config.discord) {
            throw new Error('Discord config is missing');
        }
        console.log(
            `Started refreshing application (/) commands for test guild ${this.config.discord.testGuildId}.`,
        );
        await rest.put(
            Routes.applicationGuildCommands(
                this.config.discord.clientId,
                (this.config.discord.testGuildId as string),
            ),
            {
                body: this.client.commands?.map(value => value.data.toJSON()),
            },
        );
        console.log(
            `Successfully reloaded application (/) commands for test guild ${this.config.discord.testGuildId}.`,
        );
    }

    playPreviousItem() {
        const item = this.previous.pop();
        if (!item) {
            throw Errors.EMPTY;
        }
        if (this.currentlyPlaying) {
            this.queue.unshift(this.currentlyPlaying);
        }
        return this.playItem(item);
    }

    playNextItem() {
        const item = this.queue.shift();
        if (!item) {
            throw Errors.EMPTY;
        }
        if (this.currentlyPlaying) {
            this.previous.push(this.currentlyPlaying);
            if (this.loop && !this.queue.includes(this.currentlyPlaying)) {
                this.queue.push(this.currentlyPlaying);
            }
        }
        return this.playItem(item);
    }

    private playItem(item: QueueItem) {
        const readable = this.getReadable(item);
        const resource = createAudioResource(readable);
        this.player.play(resource);
        this.currentlyPlaying = item;
        return item;
    }

    private getReadable(item: QueueItem) {
        // Song has already been buffered completely, create a new readable stream from the buffer
        if (item.buffer) {
            const readable = new Readable();
            readable.push(item.buffer);
            readable.push(null); // Signifies the end of the stream
            return readable;
        }
        let readable: Readable;
        if (item.readable.closed) {
            // Readable is closed, get a new one (since buffer is not available)
            readable = item.refresh();
        } else {
            // Readable is still open, use it
            readable = item.readable;
        }
        // Buffer the stream
        const chunks: Buffer[] = [];
        readable.on('data', (data) => {
            chunks.push(data);
        });
        readable.on('end', () => {
            item.buffer = Buffer.concat(chunks);
        });
        return readable;
    }
}
