import { spawn } from 'child_process';
import { Config } from '../config/config.type';
import { QueueItem } from '../types/queue-item.type';
export class YoutubeClient {
    constructor(private config: Config) {
    }

    public async getTitle(url: string): Promise<string> {
        return new Promise((resolve, reject) => {
            const ytdlp = spawn('yt-dlp', ['--print', 'title', url]);
            let title = '';
            ytdlp.stdout.on('data', (data) => {
                title += data.toString();
            });
            ytdlp.on('error', (error) => {
                reject(error);
            });
            ytdlp.on('close', (code) => {
                if (code !== 0) {
                    reject(new Error(`yt-dlp process exited with code ${code}`));
                } else {
                    resolve(title.trim());
                }
            });
        });
    }

    public getAudio(url: string) {
        const ytdlp = spawn('yt-dlp', ['-x', '-o', '-', url]);
        return ytdlp.stdout;
    }

    public async getPlaylist(url: string): Promise<{ title: string; items: QueueItem[] }> {
        return new Promise((resolve, reject) => {
            const items: QueueItem[] = [];
            const ytdlp = spawn('yt-dlp', ['--flat-playlist', '--dump-single-json', url]);
            let json = '';
            ytdlp.stdout.on('data', (data) => {
                json += data.toString();
            });
            ytdlp.on('error', (error) => {
                reject(error);
            });
            ytdlp.on('close', async (code) => {
                if (code !== 0) {
                    reject(new Error(`yt-dlp process exited with code ${code}`));
                } else {
                    try {
                        const playlist = JSON.parse(json);
                        for (const item of playlist.entries) {
                            const refresh = () => this.getAudio(item.url);
                            items.push({
                                title: item.title,
                                url: item.url,
                                readable: refresh(),
                                refresh,
                            });
                        }
                        resolve({ title: playlist.title, items });
                    } catch (error) {
                        reject(error);
                    }
                }
            });
        });
    }

    public isYouTubePlaylist(url: string): boolean {
        try {
            const parsedUrl = new URL(url);
            return parsedUrl.hostname.includes('youtube.com') && parsedUrl.searchParams.has('list');
        } catch (error) {
            console.error(error);
            return false;
        }
    }
}
