import { Readable } from 'stream';

export interface QueueItem {
    url: string;
    title: string;
    readable: Readable;
    buffer?: Buffer;
    refresh: () => Readable;
}
