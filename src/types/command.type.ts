import {
    CacheType,
    ChatInputCommandInteraction,
    MessageContextMenuCommandInteraction,
    SlashCommandBuilder,
    UserContextMenuCommandInteraction,
} from 'discord.js';

/**
 * Interface for discord bot command file
 */
export interface Command {
    /**
   * Command data for registering
   */
    data: SlashCommandBuilder;
    /**
   * Command execution function to be called when command is triggered
   * @param interaction interaction object
   * @returns promise
   */
    execute: (
        interaction:
            | ChatInputCommandInteraction<CacheType>
            | MessageContextMenuCommandInteraction<CacheType>
            | UserContextMenuCommandInteraction
    ) => Promise<void>;
}
