import { Client, Collection } from 'discord.js';
import { Command } from './command.type';

/**
 * Interface for discord bot client
 */
export interface CommandClient extends Client {
    /**
   * List of commands available for the bot
   */
    commands?: Collection<string, Command>;
}
