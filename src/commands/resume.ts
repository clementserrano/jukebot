import { SlashCommandBuilder } from 'discord.js';
import { Discord } from '../api/discord';
import { Command } from '../types/command.type';

export class ResumeCommand {
    /**
   * Discord slash command
   */
    private command: Command;

    constructor(context: Discord) {
        this.command = {
            data: new SlashCommandBuilder()
                .setName('resume')
                .setDescription('Resume music player'),
            async execute(interaction) {
                await interaction.deferReply();
                context.player.unpause();
                await interaction.editReply({ content: 'Player resumed ⏯' });
            },
        };
    }

    /**
   * @returns default discord slash command
   */
    get(): Command {
        return this.command;
    }
}
