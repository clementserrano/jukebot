import { AudioPlayerStatus, /* demuxProbe, */ DiscordGatewayAdapterCreator, getVoiceConnection, joinVoiceChannel, VoiceConnection, VoiceConnectionStatus } from '@discordjs/voice';
import { ChannelType, GuildMember, SlashCommandBuilder } from 'discord.js';
import { Discord } from '../api/discord';
import { Command } from '../types/command.type';
import { QueueItem } from '../types/queue-item.type';

export class PlayCommand {
    /**
     * Discord slash command
     */
    private command: Command;

    constructor(context: Discord) {
        const data = new SlashCommandBuilder()
            .setName('play')
            .setDescription('Add music to queue from url link');
        data.addStringOption(option => option.setName('url')
            .setDescription('Url link of the video/music to diffuse')
            .setRequired(true));

        this.command = {
            data,
            async execute(interaction) {
                await interaction.deferReply();
                const member: GuildMember = interaction.member as GuildMember;
                const channelId = member.voice.channelId
                    || member.guild.channels.cache.filter(channel => channel.type === ChannelType.GuildVoice && channel.members.has(member.id)).first()?.id;

                let connection: VoiceConnection = getVoiceConnection(member.guild.id) as VoiceConnection;
                // Join same channel as member
                if (channelId) {
                    const joinedConnection = joinVoiceChannel({
                        channelId: channelId,
                        guildId: member.guild.id,
                        adapterCreator: member.guild.voiceAdapterCreator as DiscordGatewayAdapterCreator,
                    });
                    // If new connection, wait for VoiceConnectionStatus.Ready
                    if (!connection) {
                        connection = joinedConnection;
                        connection.once(VoiceConnectionStatus.Ready, async () => {
                            await playAudio();
                        });
                    } else {
                        await playAudio();
                    }
                } else if (connection) {
                    await playAudio();
                } else {
                    await interaction.editReply({ content: `You must be in a channel to start the JAM 🎶 YOU STOOPID 🤪` });
                }

                async function playAudio() {
                    const player = connection.subscribe(context.player)?.player;
                    if (player) {
                        const url = interaction.options.get('url', true).value as string;
                        try {
                            if (context.youtubeApi.isYouTubePlaylist(url)) {
                                const { title, items } = await context.youtubeApi.getPlaylist(url);
                                context.queue.push(...items);
                                if (player.state.status !== AudioPlayerStatus.Playing) {
                                    context.playNextItem();
                                    await interaction.editReply(`Playing playlist ▶ "${title}" (${items.length} songs) 🎵`);
                                } else {
                                    await interaction.editReply(`Added to queue playlist ▶ ${title} (${items.length} songs) 🎵`);
                                }
                            } else {
                                const title = await context.youtubeApi.getTitle(url);
                                const refresh = () => context.youtubeApi.getAudio(url);
                                const readable = refresh();
                                // const { stream, type } = await demuxProbe(readableStream);
                                // const resource = createAudioResource(stream, { inputType: type });
                                const item: QueueItem = { title, readable, url, refresh };
                                context.queue.push(item);
                                if (player.state.status !== AudioPlayerStatus.Playing) {
                                    context.playNextItem();
                                    await interaction.editReply(`Playing music ▶ "${title}" 🎵`);
                                } else {
                                    await interaction.editReply(`Added to queue ▶ "${title}" 🎵`);
                                }
                            }
                        } catch (error) {
                            console.error(error);
                            await interaction.editReply(`Error no music matches url "${url}"`);
                        }
                    } else {
                        await interaction.editReply(`Error player not found`);
                    }
                }
            },
        };
    }

    /**
     * @returns default discord slash command
     */
    get(): Command {
        return this.command;
    }
}
