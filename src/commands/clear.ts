import { SlashCommandBuilder } from 'discord.js';
import { Discord } from '../api/discord';
import { Command } from '../types/command.type';

export class ClearCommand {
    /**
   * Discord slash command
   */
    private command: Command;

    constructor(context: Discord) {
        this.command = {
            data: new SlashCommandBuilder()
                .setName('clear')
                .setDescription('Pause and clear queue'),
            async execute(interaction) {
                await interaction.deferReply();
                context.player.stop();
                context.queue = [];
                context.previous = [];
                await interaction.editReply({ content: 'Queue cleared 🗑️' });
            },
        };
    }

    /**
   * @returns default discord slash command
   */
    get(): Command {
        return this.command;
    }
}
