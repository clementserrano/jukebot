import { SlashCommandBuilder } from 'discord.js';
import { Discord } from '../api/discord';
import { Command } from '../types/command.type';
import { Errors } from '../types/error.enum';

export class PreviousCommand {
    /**
   * Discord slash command
   */
    private command: Command;

    constructor(context: Discord) {
        this.command = {
            data: new SlashCommandBuilder()
                .setName('previous')
                .setDescription('Play previous song'),
            async execute(interaction) {
                await interaction.deferReply();
                try {
                    await interaction.editReply({ content: `Playing previous music ▶ "${context.playPreviousItem().title}"🎵` });
                } catch (error) {
                    if (error === Errors.EMPTY) {
                        await interaction.editReply({ content: `Previous queue is empty 🕳️` });
                    } else {
                        console.error(error);
                    }
                }
            },
        };
    }

    /**
   * @returns default discord slash command
   */
    get(): Command {
        return this.command;
    }
}
