import { ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder, SlashCommandBuilder } from 'discord.js';
import { Discord } from '../api/discord';
import { Command } from '../types/command.type';
import { QueueItem } from '../types/queue-item.type';

export class QueueCommand {
    /**
     * Discord slash command
     */
    private command: Command;

    constructor(context: Discord) {
        const title = 'Queue';
        const emptyQueueMessage = 'Queue is empty';
        const showFullQueueId = 'showFullQueue';

        this.command = {
            data: new SlashCommandBuilder()
                .setName('queue')
                .setDescription('Show queue list'),
            async execute(interaction) {
                await interaction.deferReply();
                const embed = new EmbedBuilder().setTitle(title).setDescription(getQueue());
                const showFullQueue = new ButtonBuilder().setCustomId(showFullQueueId).setLabel('Show full queue').setStyle(ButtonStyle.Primary);
                const actionRow = new ActionRowBuilder<ButtonBuilder>().addComponents(showFullQueue);
                const response = await interaction.editReply({ embeds: [embed], components: [actionRow] });
                try {
                    const buttonClicked = await response.awaitMessageComponent();
                    if (buttonClicked.customId === showFullQueueId) {
                        await buttonClicked.update({ embeds: [new EmbedBuilder().setTitle(title).setDescription(getFullQueue())], components: [] });
                    }
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                } catch (error) {
                    // Do nothing
                }
            },
        };

        /**
         * @returns Get last previous item and 3 next items
         */
        function getQueue(): string {
            const queue: QueueItem[] = [];
            queue.push(...context.previous.slice(-1));
            if (context.currentlyPlaying) {
                queue.push(context.currentlyPlaying);
            }
            queue.push(...context.queue.slice(0, 3));
            const concatenated = concatenateQueue(queue);
            if (queue.length === 0) {
                return emptyQueueMessage;
            } else {
                return concatenated;
            }
        }

        /**
         * @returns Get all items from previous and next queue
         */
        function getFullQueue(): string {
            const queue: QueueItem[] = [];
            queue.push(...context.previous);
            if (context.currentlyPlaying) {
                queue.push(context.currentlyPlaying);
            }
            queue.push(...context.queue);
            const concatenated = concatenateQueue(queue);
            if (queue.length === 0) {
                return emptyQueueMessage;
            } else {
                return concatenated;
            }
        }

        function concatenateQueue(queue: QueueItem[]): string {
            return queue.map((item, index) => {
                let value = `${index + 1}. ${item.title}`;
                if (item === context.currentlyPlaying) {
                    value = `**>> ${value} <<**`;
                }
                return value;
            }).join('\n');
        }
    }

    /**
   * @returns default discord slash command
   */
    get(): Command {
        return this.command;
    }
}
