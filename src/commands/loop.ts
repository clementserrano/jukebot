import { SlashCommandBuilder } from 'discord.js';
import { Discord } from '../api/discord';
import { Command } from '../types/command.type';

export class LoopCommand {
    /**
   * Discord slash command
   */
    private command: Command;

    constructor(context: Discord) {
        this.command = {
            data: new SlashCommandBuilder()
                .setName('loop')
                .setDescription('Loop current queue'),
            async execute(interaction) {
                await interaction.deferReply();
                context.loop = !context.loop;
                if (context.loop) {
                    await interaction.editReply({ content: 'Queue looped 🔁' });
                } else {
                    await interaction.editReply({ content: 'Queue loop stopped ▶️' });
                }
            },
        };
    }

    /**
   * @returns default discord slash command
   */
    get(): Command {
        return this.command;
    }
}
