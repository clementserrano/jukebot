import { SlashCommandBuilder } from 'discord.js';
import { Discord } from '../api/discord';
import { Command } from '../types/command.type';

export class ShuffleCommand {
    /**
   * Discord slash command
   */
    private command: Command;

    constructor(context: Discord) {
        this.command = {
            data: new SlashCommandBuilder()
                .setName('shuffle')
                .setDescription('Shuffle current queue'),
            async execute(interaction) {
                await interaction.deferReply();
                context.queue = context.queue.sort(() => Math.random() - 0.5);
                await interaction.editReply({ content: 'Queue shuffled 🔀' });
            },
        };
    }

    /**
   * @returns default discord slash command
   */
    get(): Command {
        return this.command;
    }
}
