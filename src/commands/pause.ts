import { SlashCommandBuilder } from 'discord.js';
import { Discord } from '../api/discord';
import { Command } from '../types/command.type';

export class PauseCommand {
    /**
   * Discord slash command
   */
    private command: Command;

    constructor(context: Discord) {
        this.command = {
            data: new SlashCommandBuilder()
                .setName('pause')
                .setDescription('Pause music player'),
            async execute(interaction) {
                await interaction.deferReply();
                context.player.pause();
                await interaction.editReply({ content: 'Player paused ⏸' });
            },
        };
    }

    /**
   * @returns default discord slash command
   */
    get(): Command {
        return this.command;
    }
}
