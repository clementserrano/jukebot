import { getVoiceConnection } from '@discordjs/voice';
import { GuildMember, SlashCommandBuilder } from 'discord.js';
import { Discord } from '../api/discord';
import { Command } from '../types/command.type';

export class StopCommand {
    /**
   * Discord slash command
   */
    private command: Command;

    constructor(context: Discord) {
        this.command = {
            data: new SlashCommandBuilder()
                .setName('stop')
                .setDescription('Stop music player and leaves voice channel'),
            async execute(interaction) {
                await interaction.deferReply();
                context.player.stop();
                const member: GuildMember = interaction.member as GuildMember;
                const connection = getVoiceConnection(member.guild.id);
                connection?.destroy();
                await interaction.editReply({ content: 'Player stopped ⏹' });
            },
        };
    }

    /**
   * @returns default discord slash command
   */
    get(): Command {
        return this.command;
    }
}
