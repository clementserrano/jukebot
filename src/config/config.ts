import fs from 'fs';
import path from 'path';
import { createGenerator } from 'ts-json-schema-generator';
import { Config } from './config.type';
import Ajv from 'ajv';

/**
 * Generate configSchema.json for JOI schema validation
 */
const generateConfig = {
    path: path.resolve(__dirname, './config.type.ts'),
    tsconfig: path.resolve(__dirname, '../tsconfig.json'),
    type: 'Config', // the interface name,
};
const schema = createGenerator(generateConfig).createSchema(generateConfig.type);
if (schema.definitions && schema.definitions.Scrap && typeof schema.definitions.Scrap !== 'boolean') {
    schema.definitions.Scrap.additionalProperties = true;
}
const validate = new Ajv().compile(schema);

/**
 * Built config from config.json file
 */
let config: Config;

/**
 * Read config.json file if present to build config object
 * See config.ts and config.example.json for configuration
 *
 * You can fork this project and override this part to implement scrap functions (not yet implemented)
 */
const filePath = `${__dirname}/../config.json`;
if (fs.existsSync(filePath)) {
    const rawdata = fs.readFileSync(filePath, 'utf8');
    const jsonData = JSON.parse(rawdata);
    // Validate your data against the schema
    if (!validate(jsonData)) {
        throw new Error('Invalid config structure : '
            + validate.errors?.map(e => `\n - In "${e.instancePath}", ${e.message}`).join(''));
    }
    config = jsonData as unknown as Config; // Cast jsonData to Config type
    console.log('Config loaded : ');
    console.log(config);
} else {
    throw new Error(`Missing ${filePath} file`);
}

export { config };
