/**
 * Typing for config.ts file
 */
export interface Config {
    /**
     * Configuration for discord bot
     */
    discord: {
        /**
         * API token for discord bot
         */
        token: string;
        /**
         * Client ID for discord bot
         */
        clientId: string;
        /**
         * Id of test guild for dev testing, if provided, commands will not be registered globally
         */
        testGuildId?: string;
    };
}
