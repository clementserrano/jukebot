FROM docker.io/bitnami/kubectl:latest AS kubectl

FROM node:lts-alpine AS node

WORKDIR /app

FROM node AS base
USER root
RUN apk update && \
    apk add --no-cache make gcc g++ python3 libtool autoconf automake yt-dlp && \
    npm i -g node-gyp && \
    rm -rf /var/cache/apk/*

FROM base AS build
COPY package.json package-lock.json ./
RUN npm ci
COPY . .
RUN npm run build

FROM base AS production
COPY --from=build /app/dist ./dist
COPY --from=build /app/node_modules ./node_modules
RUN chown -R node /app
USER node
CMD ["node", "dist/main.js"]

FROM base AS development
RUN apk add --no-cache bash git openssh-client helm && \
    npm i -g npm-check-updates && \
    rm -rf /var/cache/apk/*
COPY --from=kubectl /opt/bitnami/kubectl/bin/kubectl /usr/local/bin/kubectl
VOLUME /app